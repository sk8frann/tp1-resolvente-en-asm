%include "io.inc"
global CMAIN
global main
extern _printf
section .data
    a db 1.0
    b db 1.0
    c db -2.0
    
    cuatro dd 4
    dos dd 2
    
    sqrtResul dd 0
    negResul dd 0
    mulResul dd 0
    addX dd 0
    subX dd 0

    msgIni db 'Iniciando programa.', 0
    msgIParams db 'Inicio obtencion parametros.', 0
    msgFParams db 'Fin obtencion parametros.', 0
    msgICalc db 'Inicio calculo resolvente.', 0
    msgFCalc db 'Fin calculo resolvente.', 0
    msgResultadoPos db 'Resultado por camino positivo: %f',0
    msgResultadoNeg db 'Resultado por camino negativo: %f',0
    newLine db 10 ; salto de linea
section .text
CMAIN:
    mov ebp, esp; for correct debugging
    push msgIni
    call _printf
    push newLine
    call _printf
    add esp, 4
    mov ebp, esp; for correct debugging
    jmp main
main:
    push msgIParams
    call _printf
    push newLine
    call _printf
    enter 0,0
    mov ebp, esp
    mov dl, [ebp + 8]
    mov [a], dl
    mov edx, [ebp + 12]
    mov [b], edx
    mov edx, [ebp + 16]
    mov [c], edx
    push msgFParams
    call _printf
    push newLine
    call _printf
    jmp calcular
    
calcular:
    push msgICalc
    call _printf
    push newLine
    call _printf
    ;calculo b²
    fld DWORD [b]
    fld DWORD [b]
    fmul
    
    ;calculo 4ac y raiz cuadrada con b²
    fld DWORD [a]
    fld DWORD [c]
    fmul
    
    fild DWORD [cuatro]
    fmul
    fsub
    fsqrt
    
    fst DWORD [sqrtResul]; guardo resultado final de la raiz cuadrada
    
    fld DWORD [b]
    fchs
    fst DWORD [negResul]; guardo resultado de -b
    fadd
    
    fld DWORD [a]
    fild DWORD [dos]
    fmul
    fst DWORD [mulResul]; guardo resultado de 2a
    fdiv
    
    fstp DWORD [addX]
    
    fld DWORD [negResul]
    fld DWORD [sqrtResul]
    fsub
    fld DWORD [mulResul]
    fdiv
    
    fstp DWORD [subX]
    
    fld DWORD [addX]
    fld DWORD [subX]
    
    push addX
    push msgResultadoPos
    call _printf
    push newLine
    call _printf
     
    push subX
    push msgResultadoNeg
    call _printf
    push newLine
    call _printf
    
    push msgFCalc
    call _printf
    push newLine
    call _printf
    jmp imprimir
    
imprimir:
    xor eax, eax
    ret
    
    