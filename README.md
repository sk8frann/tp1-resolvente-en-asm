# TP1 Resolvente en ASM

#### 1. Realizar un programa para la arquitectura IA32 que calcule las raíces de una función cuadrática a través de la fórmula resolvente. Los coeficientes a, b y c de la función deben ser recibidos por parámetro. Considerar que estos podrían tomar valores de punto flotante o no.

Para la realización de este punto se empezó por crear un archivo **tp1.asm** desde el IDE SASM utilizando _el sistema operativo Windows 10_.

A continuación se muestra la sección **.data** y las variables globales definidas:

```
section .data

    ; variables para guardar parametros de entrada
    a dd 0
    b dd 0
    c dd 0

    ; variables constantes dentro de la fórmula resolvente
    cuatro dd 4
    dos dd 2
    
    ; variables para guardar resultados
    sqrtResul dd 0 ; guarda resultado de la raíz cuadrada
    negResul dd 0 ; guarda resultado -b
    mulResul dd 0 ; guarda el resultado de 2a
    addX dd 0 ; guardo el resultado de la resolvente por el camino positivo
    subX dd 0 ; guardo el resultado de la resolvente por el camino negativo
```

Veamos ahora la seccion **.text**:

```
section .text
    global _start ; funcion que se llama desde el .c
```

Por ultimo, las diferentes etiquetas de codigo:

##### Main:
```
_start:
    mov ebp, esp ; for correct debugging (agregado por SASM)
    jmp inputs ; salto a etiqueta inputs
```

##### Inputs:
```
inputs:
    ; desapilo parámetros del stack y los muevo a las variables ya definidas
    enter 0,0
    mov ebp, esp
    mov edx, [ebp + 8]
    mov [a], edx
    mov edx, [ebp + 12]
    mov [b], edx
    mov edx, [ebp + 16]
    mov [c], edx
    jmp calcular ; salto a etiqueta calcular
```

##### Calcular:
```
calcular:
    ; calculo b²
    fld DWORD [b]
    fld DWORD [b]
    fmul
    
    ; calculo 4ac-b² y raíz cuadrada al su resultado
    fld DWORD [a]
    fld DWORD [c]
    fmul
    fild DWORD [cuatro]
    fmul
    fsub
    fsqrt
    
    fst DWORD [sqrtResul]; guardo resultado final de la raíz cuadrada
    
    ; calculo el negativo de b (-b)
    fld DWORD [b]
    fchs
    fst DWORD [negResul]; guardo resultado de -b

    fadd ; sumo el resultado de la raíz cuadrada y -b
    
    ; calculo 2a
    fld DWORD [a]
    fild DWORD [dos]
    fmul
    fst DWORD [mulResul]; guardo resultado de 2a

    fdiv ; calculo la división final
    
    fstp DWORD [addX] ; guardo el resultado de la resolvente positiva
    
    fld DWORD [negResul] ; cargo el resultado de -b 
    fld DWORD [sqrtResul] ; cargo el resultado de la raíz cuadrada

    fsub ; resto el resultado de la raíz cuadrada y -b

    fld DWORD [mulResul] ; cargo el resultado de 2a

    fdiv ; calculo la division final
    
    fstp DWORD [subX] ; guardo el resultado de la resolvente negativa
```

Por otro lado, se creo un archivo **parametros.c** el cual hace un llamado a una función del archivo **.asm**:

```c
#include <stdio.h>

float a = 1;
float b = 2.0;
float c = 1.0;

extern void main(float a, float b, float c); // referencia a funcion externa

int mainC()
{
    main(a,b,c);
    return 0;
}
```
Además, se escribió un script **compile-link.sh** para compilar y linkear ambos archivos:

```sh
#!/bin/bash
nasm -f elf32 tp1.asm
gcc -m32 -o tp1 tp1.o parametros.c
```
##### **Nota:** _dentro de la carpeta donde se encuentran los archivos se incluyo la libreria io.inc necesaria para compilar el .asm._
##### **Nota2:** _fue necesario instalar nasm y MinGW para correr estos comandos en Windows._

Por utimo, para correr el programa se debe hacer lo siguiente:
```console
./compile-link.sh
./tp1
```

> El TP **no** esta terminado. Tuve dificultades para desapilar los datos del stack y luego para compilar el **.asm** y linkear el **.o** con el **.c**. Además, luego de lograr compilar y likear los archivos no logre imprimir por consola los parámetros recibidos en el .asm ni los resultados (sigo sin poder resolverlo). Por otro lado, al probar el código en _**debbug**_ y con las variables con valores "_**harcodeados**_", pude observar que los resultados en la **FPU** eran los esperados. Para finalizar, al trabarme con estos asuntos, no llegue a resolver el _segundo punto_ del trabajo practico.